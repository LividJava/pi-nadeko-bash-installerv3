#!/bin/bash
echo "Running NadekoBot"
root=$(pwd)

export DOTNET_ROOT=/usr/share/dotnet-arm64
export PATH=$PATH:/usr/share/dotnet-arm64


if hash dotnet 2>/dev/null
then
	echo "Dotnet installed."
else
	echo "Dotnet is not installed. Please install dotnet."
	exit 1
fi

cd "$root/nadekobot/output"
echo "Running NadekoBot. Please wait."
dotnet NadekoBot.dll
echo "Done"

cd "$root"
rm "$root/n-run.sh"
exit 0
