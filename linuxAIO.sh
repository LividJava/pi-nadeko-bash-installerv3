#!/bin/bash
echo ""
echo "Welcome to NadekoBot for ARM/Raspberry pi. Downloading the latest installer..."
echo "If you encounter any issues with this script join the NadekoBot Pi/ARM discord here: https://discord.gg/vjkvnaWGXQ"
root=$(pwd)

rm "$root/n-menu.sh" 1>/dev/null 2>&1
wget -N https://gitlab.com/hokutochen/pi-nadeko-bash-installerV3/-/raw/main/n-menu.sh

bash n-menu.sh
cd "$root"
rm "$root/n-menu.sh"
exit 0
